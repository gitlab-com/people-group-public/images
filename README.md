# images

This project aims to host the People Groups public images. Many of these are used within our email or issue templates which will require a publicly hosted image.

## How to link to images?

1. Navigate to the file you would like to link to. For example: https://gitlab.com/gitlab-com/people-group-public/images/-/blob/main/onboarding/welcome.png?ref_type=heads
1. Click on `Permalink` in the top right.
1. Right click on the image, and select `Open Image In New Tab`.
1. In the new tab that was opened, copy the URL from the address bar. `https://gitlab.com/gitlab-com/people-group-public/images/-/raw/daa52a869a3cfee012ba8a5e782f4072e480c26c/onboarding/welcome.png`
1. With this URL you can access the image publicly and will guarentee we have the exact version included at the time we copied the URL.

## Uploading Images

Before adding an image to this repository, be sure to confirm that the image is S.A.F.E to be shared within a public scope.

### Naming Convention

To keep things clean we aim to include images in subdirectories to allow easier maintence and categorization during this projects lifetime.

For example, adding an image only used for the onboarding process should live within the `/onboarding/` directory.

### Recommended Format

Recommended image format is [`PNG`](https://en.wikipedia.org/wiki/PNG) for consistency.